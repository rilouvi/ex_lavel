<!DOCTYPE html>
<html>
<head>
    <title>Laravel 5.8 Import Export Excel to database Example</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
</head>
<body>
   
<div class="container">
    <div class="card bg-light mt-3">
        <div class="card-header">
            Export PDF untuk User
        <div class="card-body">
            <table class="table">
              <thead class="thead-light">
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Name</th>
                  <th scope="col">Email</th>
                  <th scope="col">Created</th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $r)
                <tr>
                  <th scope="row">{{ $r->id }}</th>
                  <td>{{ $r->name }}</td>
                  <td>{{ $r->email }}</td>
                  <td>{{ $r->created_at }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>
    </div>
</div>
   
</body>
</html>